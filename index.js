module.exports = {
  assign: require('./lib/assign'),
  delete: require('./lib/delete'),
  get: require('./lib/get'),
  has: require('./lib/has'),
  set: require('./lib/set'),
  Map: require('./lib/map')
};
