'use strict';

const assign = require('./assign');
const remove = require('./delete');
const get = require('./get');
const has = require('./has');
const set = require('./set');

class DeepMap {
  constructor() {
    this.values = {};
  }

  assign(values) {
    assign(this.values, values);
    return this;
  }

  clear() {
    this.values = {};
    return this;
  }

  delete(key) {
    remove(this.values, key);
    return this;
  }

  get(key) {
    return get(this.values, key);
  }

  has(key) {
    return has(this.values, key);
  }

  set(key, value) {
    set(this.values, key, value);
    return this;
  }

  toJSON() {
    return this.values;
  }

  clone() {
    return new DeepMap()
      .assign(this.values);
  }
}

module.exports = DeepMap;
